import { Flex, Button, Row, Col } from "antd";
import { Link } from "react-router-dom";

const Home = () => {
    return (
        <Row>
            <Col span={8}></Col>
            <Col span={8}>
                <Flex gap={'middle'} vertical={true} justify="center" align="center">
                    <h1>Home</h1>
                    <Link to="/signin">
                        <Button>Signin</Button>
                    </Link>
                    <Link to="/signup">
                        <Button>Signup</Button>
                    </Link>
                </Flex>
            </Col>
            <Col span={8}></Col>
        </Row>
    );
}

export default Home;