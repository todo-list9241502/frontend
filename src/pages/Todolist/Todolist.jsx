import { Flex, Row, Col, Divider, List, Button } from "antd";
import { Link, useNavigate } from "react-router-dom";
import { useState, useRef, useEffect } from "react";
import * as callApi from '../../utils/callApi';
import Listitem from "./Compoments/Listitem.jsx";
import Todoinput from "./Compoments/Todoinput.jsx";

const Todolist = () => {
    const [todos, setTodos] = useState([]);
    const [todoInput, setTodoInput] = useState('');
    const navigate = useNavigate();

    const todoInputRef = useRef(null);

    const onClickAddTodo = () => {
        if (todoInput === '') { return; }
        const data = {
            content: todoInput,
            isDone: false,
        }
        createTodo(data);
        setTodoInput('');
        todoInputRef.current.focus();
    }

    const createTodo = async (data) => {
        const todolist = await callApi.post('/todolist', data);
        if (todolist.statusCode === 200) {
            const todo = {
                id: todolist.result.id,
                content: todolist.result.content,
                isDone: false,
            }
            setTodos([...todos, todo]);
        }
    };

    const onClickDone = (id) => {
        updateTodo(id, { isDone: true });
    };

    const updateTodo = async (id, data) => {
        const todolist = await callApi.put(`/todolist/${id}`, data);
        if (todolist.statusCode === 200) {
            const newTodos = todos.map((todo) => {
                if (todo.id === id) {
                    return {
                        ...todo,
                        isDone: true,
                    };
                }
                return todo;
            });
            setTodos(newTodos);
        }
    };

    const onClickDelete = async (id) => {
        await callApi.remove(`/todolist/${id}`);
        setTodos(todos.filter((todo) => todo.id !== id));
    }

    useEffect(() => {
        getTodolists();
    }, []);

    const getTodolists = async () => {
        const todolists = await callApi.get('/todolist');
        if (todolists.statusCode === 401) {
            navigate('/signin');
        }
        setTodos(todolists.result);
    };

    return (
        <Row>
            <Col span={4}></Col>
            <Col span={16}>
                <Flex gap={'middle'} vertical align="center" justify="center" style={{ width: "100%" }}>
                    <h1>Todolist</h1>
                    <Divider />
                    <List
                      style={{ width: '100%' }}
                      size="large"
                      bordered
                      dataSource={todos}
                      renderItem={(item) => 
                        <List.Item>
                            <Listitem
                              item={item}
                              onClickDone={onClickDone}
                              onClickDelete={onClickDelete}
                            />
                        </List.Item>
                      }
                    />
                    <Todoinput
                      todoInput={todoInput}
                      todoInputRef={todoInputRef}
                      setTodoInput={setTodoInput}
                      onClickAddTodo={onClickAddTodo}
                    />
                </Flex>
            </Col>
            <Col span={4}>
                <Flex gap={'middle'} vertical align="center" justify="center" style={{ width: "100%" }}>
                    <Link to="/">
                        <Button>logout</Button>
                    </Link>
                </Flex>
            </Col>
        </Row>
    );
}

export default Todolist;