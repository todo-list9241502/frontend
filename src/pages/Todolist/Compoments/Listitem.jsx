import { Flex, Button } from "antd";

const Listitem = ({ item, onClickDone, onClickDelete }) => {
    const { id, content, isDone } = item;
    return (
        <>
            {
                isDone
                ? <span>{content+' - (done)'}</span>
                : <span>{content}</span>
            }
            <Flex gap={'small'} justifycontent="space-between" alignitems="center">
                <Button onClick={() => onClickDone(id)}>Done</Button>
                <Button danger onClick={() => onClickDelete(id)}>Delete</Button>
            </Flex>
        </>
    );
};

export default Listitem;