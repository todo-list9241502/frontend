import { Flex, Input, Button } from "antd";

const Todoinput = ({ todoInput, todoInputRef, setTodoInput, onClickAddTodo }) => {
    return (
        <Flex gap={'small'} style={{ width: '100%' }}>
            <Input placeholder="todo" size="middle" value={todoInput} ref={todoInputRef} onChange={(e) => setTodoInput(e.target.value)}/>
            <Button type="primary" onClick={onClickAddTodo}>add todo</Button>
        </Flex>
    );
};

export default Todoinput;