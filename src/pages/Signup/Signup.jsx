import { Flex, Row, Col, Input, Button } from "antd";
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useRef } from "react";
import * as callApi from '../../utils/callApi';

const Signup = () => {
    const inputAccountRef = useRef();
    const inputPasswordRef = useRef();
    const navigate = useNavigate();

    useEffect(() => {
        inputAccountRef.current.focus();
    }, [])

    const clickSignupButton = async () => {
        const account = inputAccountRef.current.input.value;
        const password = inputPasswordRef.current.input.value;
        if (account === '' || password === '') {
            alert('account or password is empty');
            return;
        }

        const data = {
            account: account,
            password: password
        }

        const result = await signup(data);
        alert(result);
        navigate('/signin');
    }

    const signup = (data) => {
        return callApi.post('/user', data);
    }

    return (
        <Row>
            <Col span={8}></Col>
            <Col span={8}>
                <Flex gap="middle" vertical={true} justify="center" align="center">
                    <h1>Signup</h1>
                    <Input placeholder="account" style={{ width: '50%' }} ref={inputAccountRef}/>
                    <Input placeholder="password" style={{ width: '50%' }} ref={inputPasswordRef}/>
                    <Flex gap="middle" vertical style={{ width: '50%' }}>
                        <Button type="primary" block={true} onClick={clickSignupButton}>Signup</Button>
                        <Link to="/signin">
                            <Button block={true}>signin</Button>
                        </Link>
                        <Link to="/">
                            <Button block={true}>home</Button>
                        </Link>
                    </Flex>
                </Flex> 
            </Col>
            <Col span={8}></Col>
        </Row>
    );
}

export default Signup;