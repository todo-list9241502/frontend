import { Flex, Row, Col, Input, Button } from "antd";
import { Link, useNavigate } from "react-router-dom";
import { useEffect, useRef } from "react";
import * as callApi from '../../utils/callApi';

const Signin = () => {
    const inputAccountRef = useRef();
    const inputPasswordRef = useRef();
    const navigate = useNavigate();

    useEffect(() => {
        inputAccountRef.current.focus();
    }, [])

    const clickSigninButton = async () => {
        const account = inputAccountRef.current.input.value;
        const password = inputPasswordRef.current.input.value;

        const data = {
            account: account,
            password: password
        }

        const result = await signin(data);
        if (result.statusCode === 200) {
            navigate('/todolist');
            return;
        } else {
            alert(result);
        }
    }

    const signin = (data) => {
        return callApi.post('/user/signin', data);
    }

    return (
        <Row>
            <Col span={8}></Col>
            <Col span={8}>
                <Flex gap="middle" vertical={true} justify="center" align="center">
                    <h1>Signin</h1>
                    <Input placeholder="account" style={{ width: '50%' }} ref={inputAccountRef}/>
                    <Input placeholder="password" style={{ width: '50%' }} ref={inputPasswordRef}/>
                    <Flex gap="middle" vertical style={{ width: '50%' }}>
                        <Button type="primary" block={true} onClick={clickSigninButton}>signin</Button>
                        <Link to="/signup">
                            <Button block={true}>signup</Button>
                        </Link>
                        <Link to="/">
                            <Button block={true}>home</Button>
                        </Link>
                    </Flex>
                </Flex> 
            </Col>
            <Col span={8}></Col>
        </Row>
    );
}

export default Signin;