import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home/Home.jsx";
import Signin from "./pages/Signin/Signin.jsx";
import Signup from "./pages/Signup/Signup.jsx";
import Todolist from "./pages/Todolist/Todolist.jsx";
import Page404 from "./pages/Page404/Page404.jsx";


function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/signin" element={<Signin />} />
      <Route path="/signup" element={<Signup />} />
      <Route path="/todolist" element={<Todolist />} />
      <Route path="*" element={<Page404 />} />
    </Routes>
  );
}

export default App;
