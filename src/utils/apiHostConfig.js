let apiUrl;
const hostname = window && window.location && window.location.hostname;

if(hostname === 'whatsgoodonmenu.com') {
  apiUrl = 'https://api.whatsgoodonmenu.com';
} else {
  apiUrl = 'http://localhost:3030/api/v1';
}

export default apiUrl;