import axios from "axios";
import apiUrl from "./apiHostConfig";

axios.defaults.withCredentials = true;

export const get = (url) => {
    return axios({
        method: 'GET',
        url: `${apiUrl}${url}`,
        validateStatus: () => true,
    }).then(response => response.data);
};

export const post = (url, data) => {
    return axios.post(`${apiUrl}${url}`, data)
            .then(response => {
                return response.data;
            })
            .catch(error => {
                return error;
            });
};

export const put = (url, data) => {
    return axios.put(`${apiUrl}${url}`, data)
            .then(response => {
                return response.data;
            })
            .catch(error => {
                return error;
            });
}

export const remove = (url) => {
    return axios.delete(`${apiUrl}${url}`)
            .then(response => {
                return response.data;
            })
            .catch(error => {
                return error;
            });
}