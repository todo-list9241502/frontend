FROM node:20-alpine AS builder
WORKDIR /app
COPY package*.json .
RUN npm install
COPY ./src ./src
COPY ./public ./public
RUN npm run build

FROM nginx
COPY ./nginx.conf /etc/nginx/nginx.conf 
COPY --from=builder /app/build /usr/share/nginx/html
